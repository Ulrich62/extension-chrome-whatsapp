let btn = document.getElementById("getCurrentTab");


if (btn !== null) {
  btn.addEventListener("click", getCurrentTab);
};

async function getCurrentTab() {

  //Get current Tab

  await chrome.tabs.query({ active: true, lastFocusedWindow: true }, async tabs => {
    let tab = tabs[0].id;

    //Get scraped data

    await chrome.tabs.executeScript(tab.id, { code: 'document.documentElement.outerHTML' }, scraped => {

      //Find occurencies of some classes in scraped[0] (a string)

      let getHtml = document.getElementById("Total messages");
      getHtml.innerHTML = (scraped[0].match(new RegExp("_2nY6U vq6sj", "g")).length);

      let getHtml2 = document.getElementById("Message non lus");
      getHtml2.innerHTML = (scraped[0].match(new RegExp("_1pJ9J", "g")).length);

      let getHtml3 = document.getElementById("Message lus");
      getHtml3.innerHTML = ((scraped[0].match(new RegExp("_2nY6U vq6sj", "g")).length - scraped[0].match(new RegExp("_1pJ9J", "g")).length));
    });
  });
};


let btn2 = document.getElementById("exportCsv");

if (btn2 !== null) {
  btn2.addEventListener("click", exportCsv);
};


async function exportCsv() {

  //Get current Tab

  await chrome.tabs.query({ active: true, lastFocusedWindow: true }, async tabs => {
    let tab = tabs[0].id;

    //Get scraped data

    await chrome.tabs.executeScript(tab.id, { code: 'document.documentElement.outerHTML' }, scraped => {

      //Convert string to html document 

      let parser = new DOMParser();

      let htmldoc = parser.parseFromString(scraped[0], "text/html");

      //Fill CSV array with scraped data. 

      let csvarrayName = [];

      for (let i = 0; i < htmldoc.getElementsByClassName("zoWT4").length; i++) {
        let eachCsvArrayName = [];
        eachCsvArrayName.push(htmldoc.getElementsByClassName("zoWT4")[i].childNodes[0].childNodes[0].nodeValue);
        eachCsvArrayName.push(htmldoc.getElementsByClassName("_3vPI2")[i].childNodes[1].childNodes[0].nodeValue);
        csvarrayName.push(eachCsvArrayName);
      };

      //Fill html table

      var table = document.getElementById('myTable');

      for (var i = 0; i < csvarrayName.length; i++) {
        var row = `<tr>
							<td>${csvarrayName[i][0]}</td>
							<td>${csvarrayName[i][1]}</td>
					  </tr>`
        table.innerHTML += row
      };

      //Export CSV file

      let csvContent = "data:text/csv;charset=utf-8,"
        + csvarrayName.map(e => e.join(",")).join("\n");

      var link = document.createElement("a");
      var title = document.createTextNode("Get CSV file");
      link.appendChild(title);
      link.setAttribute("href", csvContent);
      link.setAttribute("class", "button");
      document.body.appendChild(link);
    });
  });

};


